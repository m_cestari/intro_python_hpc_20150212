__author__ = 'mirko'


import sys
import glob
import time
import datetime

from mpi4py import MPI
from subprocess import Popen

VERBOSE = 0

def input_files(my_path):
    """
    Get the input names

    :param my_path: path of the input files
    :return: a list of inputs
    """
    return glob.glob(my_path+'/*')


def run(my_inputs):
    """
    Run cash_flow for all inputs by using Popen

    :param my_inputs: list of input files
    :return:
    """
    for input_ in my_inputs:
        with open(input_+'.out', 'w') as out_file:
            if VERBOSE:
                print "./cash_flow_dbg.x %s" % input_
            pid = Popen(['./cash_flow_dbg.x', input_], stdout=out_file)
            pid.wait()



if __name__ == '__main__':

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    comm.Barrier()
    t1 = time.time()

    if rank == 0:
        # welcome message
        print '*'*80
        print datetime.datetime.now()
        print '*'*80


    # get all the input names
    inputs = input_files(sys.argv[1])

    if rank == 0:
        print "Number of elaborations: %d" % len(inputs)

    # sort the elaborations
    inputs = sorted(inputs)

    # assign each task its portion of input files
    queue = []
    for i in range(0+rank, len(inputs), size):
        queue.append(inputs[i])

    print "Rank: %d, # elaborations %d " % (rank, len(queue))

    # print the 300K elaborations for each task
    # for inp in queue:
    #    if '300K' in inp:
    #       print "rank: %d %s" % (rank, inp)

    run(queue)

    tt = time.time()
    print "Rank: %d, Elapsed time %5.2f " % (rank, tt-t1)


    comm.Barrier()
    t2 = time.time()
    if rank == 0:
        print '*'*80
        print '%s Elapsed time: %5.2f ' % (datetime.datetime.now(), t2-t1)
        print '*'*80
