__author__ = 'mirko'


import sys
import glob
import time
import datetime

from subprocess import Popen

VERBOSE = 1

def input_files(my_path):
    """
    Get the input names

    :param my_path: path of the input files
    :return: a list of inputs
    """
    return glob.glob(my_path+'/*')


def run(my_inputs):
    """
    Run cash_flow for all inputs by using Popen

    :param my_inputs: list of input files
    :return:
    """
    for input_ in my_inputs:
        with open(input_+'.out', 'w') as out_file:
            if VERBOSE:
                print "./cash_flow_dbg.x %s" % input_
            pid = Popen(['./cash_flow_dbg.x', input_], stdout=out_file)
            pid.wait()



if __name__ == '__main__':

    # welcome message
    print '*'*80
    print datetime.datetime.now()
    print '*'*80

    t1 = time.time()

    # get all the input names
    inputs = input_files(sys.argv[1])

    print "Number of elaborations: %d" % len(inputs)

    # print inputs

    run(inputs)


    t2 = time.time()
    print '*'*80
    print '%s Elapsed time: %5.2f ' % (datetime.datetime.now(), t2-t1)
    print '*'*80
